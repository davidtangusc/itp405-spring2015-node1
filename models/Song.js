var sequelize = require('./../config/sequelize');
var Sequelize = require('sequelize');
var Validator = require('validatorjs');

var Song = sequelize.define('song', {
	title: {
		type: Sequelize.STRING,
		field: 'title'
	},
	playCount: {
		type: Sequelize.INTEGER,
		field: 'play_count'
	}
}, {
	timestamps: false
});

Song.validate = function(input) {
	return new Validator(input, {
		title: 'required'
	});
};

module.exports = Song;
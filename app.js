var express = require('express');
var app = express();
var ejs = require('ejs');
var Song = require('./models/Song');

app.set('view engine', 'ejs');

// respond with "hello world" when a GET request is made to the homepage
app.get('/', function(req, res) {
	res.render('index', {
		title: 'Music Search'
	});
});

app.get('/songs', function(req, res) {
	var validation = Song.validate({ title: req.query.title });

	if (validation.fails()) {
		console.log(validation.errors.first('title'));
		return res.redirect('/');
	}

	// console.log(req.query.title)
	Song.findAll({
		where: {
			title: { like: '%' + req.query.title + '%' },
			playCount: { gt: 0 }
		},
		order: 'playCount DESC'
	}).then(function(results) {
		res.render('songs', {
			title: 'Songs',
			songs: results
		});
	});
});

app.listen(3000, function() {
  console.log('Listening at http://localhost:3000');
});